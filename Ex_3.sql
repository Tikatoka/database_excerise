--3.1.a
Select concat(e.fnam, ' ', e.lname) as name
from employee e, works_on w, project p
Where
p.name='Prodect Mega' and p.num=w.pno and e.ni=w.eni and e.dno=5 and w.hours>10


--3.1.b
select 
if(w.eni=e.ni and w.pno=p.pnum and p.pname='Product Alfa', e.salary * 1.1, e.salary) as salary 
from employee e, project p, works_on w

--3.1.c
select
concat(e.fnam, ' ', e.lname) as name
from
employee e, dependent d
where
e.ni=d.eni and instr(d.dependent_name, e.fnam)=1

--3.1.d
select
concat(e.fnam, ' ', e.lname) as name, p.pname as project
from
employee e, project p, works_on w, department d
where
e.ni=w.eni and w.pno=p.pnum and d.dnum=e.dno
order by
d.dname, e.lname, e.fnam

--3.1.e
select (e.fnam, ' ', e.lname) as name
from
employee e, employee d
where
e.superni=d.ni and d.fnam='Franklin' and d.lname='Wong'


--3.2.a
select name from student where class=4 and major='CS'

--3.2.b
select 
c.course_name
from
course c, section s
where
s.instructor='King' and (s.year=07 or s.year=08) and s.course_number=c.course_number

--3.2.c
select s.course_number, s.semester, s.year, count(
    select student_number 
    from grade_report g, section s
    where s.instructor='King' and s.section_identifier = g.section_identifier
    ) as number_of_student
from section s
where s.instructor='King'

--3.2.d

select 
s.name, c.course_name, c.course_number, c.credit_hours, sec.semester, sec.year, g.grade
from
student s, grad_report g, section sec, course c 
where 
s.major='CS' 
and s.class=4
and s.student_number = g.student_number 
and g.section_identifier = s.section_identifier 
and g.course_number = c.course_name


--3.2.e
select 
s.instructor, c.course_name, c.course_number
from
section s, course c
where 
s.course_number = c.course_number